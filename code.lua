--[[
    Super Key World
    Created by Anthony Vernon AKA FurthestChunk
    BG texture by squaredotcube from the LRR Discord


    Utilising the Micro Platformer framework created by Matt Hughson
    (@matthughson | http://www.matthughson.com/)

]]--

--player information
p1=
{
	--position, representing the top left of
	--of the player sprite.
	x=60,
	y=32,
	--velocity
	dx=0,
	dy=0,
    --facing, 1/false is right, 0/true is left
    --Yes I know 1/false looks stupid
    facing = 1,	
    flip = false,
	--is the player standing on
	--the ground. used to determine
	--if they can jump.
	isgrounded=false,
	
	--how fast the player is launched
	--into the air when jumping.
	jumpvel=3.0,
}

--screen info
screen = {
	scrollX = 0,
    checkpointX = 60,
    checkpointY = 32,
    checkpointScrollX = 0,
}


--globals
g=
{
	grav=0.1, -- gravity per frame
}

--checkpoint and the door
local checkpointIds = {8, 9, 24, 25, 39, 40}
local pointsChecked = 0;
local checkpoints = 
{
    {sprites = checkpointIds, width = 2, x=888, y = 96, offset = 0},
    {sprites = checkpointIds, width = 2, x=1656, y = 96, offset = 0},
    {sprites = checkpointIds, width = 2, x=16560, y = 96, offset = 0}
}

local doorIds = {181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192}
local door = {sprites=doorIds, width = 3, x=1968,y=56}

local flag = 0 -- stores the flag globally since it's used every frame
local control = false
local controlTimer = 0
local dashAllowed = true
local dashTimer = 0
local deltaX = 1
local points = 0

--title screen
local scrollTimer = 0
local titleTimer = 0
title = true
ending = false

-- this is the title screen's logo
line1 = {44, 45, 46, 47}
line2 = {48, 49, 50, 51, 52, 53, 54, 55, 56}
line3 = {57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67}
line4 = {68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80}
line5 = {81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94}
line6 = {95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109}
line7 = {110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122}
line8 = {123, 124, 125, 126, 127, 128, 15, 15, 129, 130}
line9 = {131, 132, 15, 15, 15, 15, 15, 133, 134}
line10 = {135, 136}
line11 = {137, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 138}
line12 = {139, 140, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 141, 142}
line13 = {143, 144, 15, 15, 15, 15, 15, 15, 15, 15, 145, 146, 147, 147, 148, 149, 150}
line14 = {150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165}
line15 = {166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180}

--resets player position and info
function Reset()
    PlaySound(6,1)
    screen.scrollX = screen.checkpointScrollX
    ScrollPosition(screen.scrollX, 0)
    p1.x = math.floor(screen.checkpointX - 8)
    p1.y = math.floor(screen.checkpointY)
    p1.dy = 0
    p1.dx = 0
    p1.facing = 1
    dashAllowed = true
    dashTimer = 0
    control = false
end

--called once at the start of the program.
function Init()
	BackgroundColor(0)
    control = false
end


--called 60 times per second
function Update(deltaTime)

    ScrollPosition(screen.scrollX, 0)
    --checks special conditions first
    if title == true then
        control = false
        ScrollPosition (scrollTimer,128)
        if scrollTimer == 0 and (Button(Buttons.A) or Button(Buttons.B)) then
            scrollTimer = 1
        end
        if scrollTimer < 128 and scrollTimer > 0 then
            scrollTimer +=1
        end
        if scrollTimer == 128 and (Button(Buttons.A) or Button(Buttons.B)) then
            title = false
            PlaySong( {0, 1, 2}, true) 
        end
    end
    
    if titleTimer > 20 and title == false then
        titleTimer +=1
    end
    if titleTimer == 20 then
        control = true
    end

    if ending == true then
        ScrollPosition (384-scrollTimer,128)
        if scrollTimer < 128 then
            scrollTimer +=1
        end
        control = false
    end

    --if dashing happens, do the speed thing
    if dashAllowed == false then
        dashTimer +=1
    end
    if dashTimer == 50 then
        dashTimer = 0
        dashAllowed = true
    end

	--remember where we started
	local startx=p1.x
	
	--if the player is in control
	if control then
        --do the dash!
        if (Button(Buttons.B) or Button(Buttons.Down)) and p1.isgrounded and dashAllowed then
            dashAllowed = false
            deltaX = 1.5
            PlaySound(5, 1)
        end
        --jump them!
	    if (Button(Buttons.Up) or Button(Buttons.A))
    	 and p1.isgrounded then
		    p1.dy=-p1.jumpvel
            PlaySound(4, 1)
    	end
	
    	--shuffle dat key!
	    p1.dx=0
    	if Button(Buttons.Left) then --left
            p1.facing = -1
    		p1.dx=-1
    	end
    	if Button(Buttons.Right) then --right
            p1.facing = 1
    		p1.dx=1
    	end
    end

    -- returns control afterabit
    if control == false and (ending == false or title == false) then
        controlTimer += 1
    end
    if controlTimer == 20 then
        control = true
        controlTimer = 0
    end

    -- handles dash speed
    dashEnd = p1.dx
    if dashTimer >= 1 then
        p1.dx = p1.facing*1.25
    end
    if dashTimer >= 7 then
        p1.dx = p1.facing*1.5
    end
    if dashTimer >= 15 then
        p1.dx = p1.facing*2
    end
    if dashTimer >= 22 then
        p1.dx = p1.facing*1.25
    end
    if dashTimer >= 30 then
        p1.dx = p1.facing*1.5
    end
    if dashTimer >= 35 then
        p1.dx = p1.facing*1.25
    end
    if dashTimer >= 40 then
        p1.dx = dashEnd
    end
	--move the player left/right
	p1.x=p1.x+p1.dx
	
	--hit side walls
	--also checks instant death, checkpoints and THE DOOR
	
	--check for walls in the
	--direction we are moving.
	local xoffset=0 --moving left check the left side of sprite.
	if p1.dx>0 then xoffset=7 end --moving right, check the right side.
	
	--look for a wall on either the left or right of the player
	--and at the players feet.
	--We divide by 8 to put the location in TileMap space (rather than
	--pixel space).
	flag=Flag(((p1.x+xoffset+screen.scrollX)/8),(p1.y+7)/8)
	--We use flag 0 (solid black) to represent solid walls. This is controlled 
	--by tilemap-flags.png.
	if flag==0 then
		--they hit a wall so move them
		--back to their original pos.
		--it should really move them to
		--the edge of the wall but this
		--mostly works and is simpler.
		p1.x=startx
    end
    if flag==8 then
        ending = true
        scrollTimer = 0
        StopSong()
    end
    if flag==6 then
        if checkpoints[points+1].offset == 0 and (p1.x+screen.scrollX) >= checkpoints[points+1].x then
            checkpoints[points+1].offset = 1
            points +=1
        end
        screen.checkpointX=math.floor(p1.x+xoffset)
        screen.checkpointScrollX=math.floor(screen.scrollX)
    end
    if flag==7 then
        Reset()
    end
    
    -- handles screen scrolling
    if p1.x >= 65 then
        p1.x=64
        screen.scrollX += p1.dx
    end
	
    if p1.x <= 35 and screen.scrollX > 0 then
        p1.x=36
        screen.scrollX += p1.dx
    end

	--accumulate gravity
	p1.dy=p1.dy+g.grav
	
	--apply gravity to the players position.
	p1.y=p1.y+p1.dy

	--hit floor
	--also handles instant death
	
	--assume they are floating 
	--until we determine otherwise
	p1.isgrounded=false
	
	--only check for floors when
	--moving downward
	if p1.dy>=0 then
		--check bottom center of the
		--player.
		flag=Flag((p1.x+4+screen.scrollX)/8,(p1.y+8)/8)
		--look for a solid tile
        if flag==7 then
            Reset()
        end
		if flag==0 then
			--place p1 on top of tile
			p1.y = math.floor((p1.y)/8)*8
			--halt velocity
			p1.dy = 0
			--allow jumping again
			p1.isgrounded=true
		end
	end
	
	--hit ceiling
	--also handles instant death
		
	--only check for ceilings when
	--moving up
	if p1.dy<=0 then
		--check top center of player
		flag=Flag((p1.x+4+screen.scrollX)/8,(p1.y)/8)
		--look for solid tile
		if flag==0 then
			--position p1 right below
			--ceiling
			p1.y = math.floor((p1.y+8)/8)*8
			--halt upward velocity
			p1.dy = 0
		end
        if flag==7 then
            Reset()
        end
	end
    if p1.facing == 1 then
        p1.flip = false
    else
        p1.flip = true
    end
end

function Draw()
	--clear the screen so we start each frame
	--with a blank canvas to draw on.
 	RedrawDisplay()
	--draw the player, dash indicator, checkpoints and DOOR
    if (title == false and ending == false) then
        if dashTimer >=1 then
            DrawSprite(4,p1.x+(-8*p1.facing),p1.y,p1.flip)
        end
	    DrawSprite(5,p1.x,p1.y, p1.flip, false, DrawMode.SpriteAbove)--draw player

        checkTotal = #checkpoints
        for i = 1, checkTotal do
            local point = checkpoints[i]
            DrawSprites(point.sprites, point.x, point.y, point.width, false, false, DrawMode.Sprite, point.offset, true, true)
        end
        DrawSprites(door.sprites, door.x, door.y, door.width, false, false, DrawMode.Sprite, 0, true, true)
    end

    --draws the title screen
    if title == true then
        DrawSprites(line1, 64, 128, 4)
        DrawSprites(line2, 32, 136, 10)
        DrawSprites(line3, 24, 144, 12)
        DrawSprites(line4, 16, 152, 13)
        DrawSprites(line5, 16, 160, 14)
        DrawSprites(line6, 8, 168, 15)
        DrawSprites(line7, 0, 176, 13)
        DrawSprites(line8, 0, 184, 10)
        DrawSprites(line9, 8, 192, 9)
        DrawSprites(line10, 8, 200, 2)
        DrawSprites(line11, 0, 208, 16)
        DrawSprites(line12, 0, 216, 16)
        DrawSprites(line13, 0, 224, 16)
        DrawSprites(line14, 0, 232, 16)
        DrawSprites(line15, 8, 240, 16)
        DrawText("Press X or C",16,248,DrawMode.TilemapCache)

        DrawText("A wizard has", 128, 128, DrawMode.TilemapCache)
        DrawText("cursed you for", 128, 136, DrawMode.TilemapCache)
        DrawText("not returning", 128, 144, DrawMode.TilemapCache)
        DrawText("his lawnmower!", 128, 152, DrawMode.TilemapCache)
        DrawText("Go unlock your", 128, 160, DrawMode.TilemapCache)
        DrawText("heart door to", 128, 168, DrawMode.TilemapCache)
        DrawText("break the curse!", 128, 176, DrawMode.TilemapCache)
        DrawText("Controls:", 128, 192, DrawMode.TilemapCache)
        DrawText("Left/Right to go", 128, 200, DrawMode.TilemapCache)
        DrawText(" left or right", 128, 208, DrawMode.TilemapCache)
        DrawText("Up/X to jump", 128, 216, DrawMode.TilemapCache)
        DrawText("Down/C to dash", 128, 224, DrawMode.TilemapCache)
        DrawText("Press X or C", 144, 240, DrawMode.TilemapCache)
        DrawText("to start!", 156, 248, DrawMode.TilemapCache)
    end
    
    --draws end credits
    if ending == true then

        DrawText("You have broken", 256, 128, DrawMode.TilemapCache)
        DrawText("the wizard's", 256, 136, DrawMode.TilemapCache)
        DrawText("curse!", 256, 144, DrawMode.TilemapCache)
        DrawText("You have proven", 256, 152, DrawMode.TilemapCache)
        DrawText("yourself!", 256, 160, DrawMode.TilemapCache)
        DrawText("When you were", 256, 176, DrawMode.TilemapCache)
        DrawText("on your journey", 256, 184, DrawMode.TilemapCache)
        DrawText("the wizard let", 256, 192, DrawMode.TilemapCache)
        DrawText("himself into", 256, 200, DrawMode.TilemapCache)
        DrawText("your house and", 256, 208, DrawMode.TilemapCache)
        DrawText("stole your TV.", 256, 216, DrawMode.TilemapCache)
        DrawText("What a jerk.", 256, 232, DrawMode.TilemapCache)
        DrawText("THE END", 292, 248, DrawMode.TilemapCache)
    end
end
